# KiCAD InPulse Libraries

Reposit�rio destinado �s libraries do KiCAD para os projetos da InPulse.

Para utiliz�-lo no KiCAD deve-se configurar os "paths" do programa para as 
pastas contidas dentro do reposit�rio. No KiCAD:

- Preferences -> Configure Paths:

  KICAD_SYMBOL_DIR = "path_do_repositorio/inpulse_symbol_library"
  KISYSMOD = "path_do_repositorio/inpulse_footprints"
  KISYS3DMOD = "path_do_repositorio/inpulse_footprints/packages3d"

H� tamb�m no reposit�rio dois arquivos (fp_lib_table e sym_lib_table) que 
devem ser copiados para a pasta local onde o kicad l� as tabelas de libraries. 

ATEN��O: fazer isso SEMPRE que der um PULL no git. Fazer tamb�m o inverso quando for 
dar um PUSH (ou seja, copie os arquivos de sua pasta local para o reposit�rio do git),
caso contr�rio, as tabelas n�o ser�o atualizadas.

A pasta local pode ser encontrada em:

- Symbol editor -> preferences -> manage libraries -> global libraries -> FILE: "path"

- Footprint editor -> preferences -> manage libraries -> global libraries -> FILE: "path"

